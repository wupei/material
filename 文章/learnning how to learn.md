Learning How to learn (字幕中英对照版)

翻译计划（把所有的字幕翻译一遍，形成整体的文章）

0:01
Welcome to Learning How to Learn. 
0:04
Your brain has amazing abilities, but it didn't come with an instruction manual. Perhaps the greatest gif that our brains give us is the ability to learn new things everyday. 
0:15
On my way here, I thought about the journey that will take us to the last day of the course and how much we will learn along the way. Our goal is to give you a better understanding of how we learn, so that your brain becomes a better learner. 
0:29
These insights are based on solid research from neuroscience, from cognitive psychology, and also from dozens of leading instructors and practitioners in difficult-to-learn subjects. Whether you're a novice or an expert, you will find great new ways to improve your skills and techniques for learning, especially related to math and science. 
0:50
This course is meant to help you reframe how you think about learning, to help reduce your frustration and increase your understanding. We approach things a little differently. You're not expected to have an in-depth background in any particular subject. Instead, you're expected to take these ideas and apply them to whatever subject you're trying to learn or improve in, to help you learn more deeply, effectively, and with less frustration. You'll hear experts from a variety of different disciplines talking about their best tips for learning more effectively. You can benefit from these ideas whether you are struggling in high school or soaring through math and science at graduate levels at a university. 
1:35
I'm a co-director of a science and learning center that is sponsored by the National Science Foundation, based here in La Jolla. In recent years, we've made great strides from research, in discovering how to learn most effectively. Finding a way to simply and effectively share these ideas with you, has been a big undertaking, but we feel it's well worth doing. You'll see that many of these ideas, although simple, are incredibly powerful. And along the way, we'll also learn a lot in the process of teaching you. 

2:06
You'll see how you can fool yourself about whether you actually know the material. You'll discover new ways to hold your focus and embed the material more deeply and powerfully in your mind. And you'll learn to condense key ideas you're learning about, so you can grasp them more easily, master the simple, practical approaches outlined here, including simple tips to help prevent procrastination. And you'll be able to learn more effectively and with less frustration. This course is meant to enrich both your learning and your life. You'll able to get what you want from this material. So, welcome to the course, and happy learning. 
下载

课程视频mp4
字幕 (Chinese (Simplified))
WebVTT
字幕全文 (Chinese (Simplified))
txt
Course Structure Script.pdf
pdf